package Bhrytsak;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class Weather {
    private String zip;

    JsonElement jse;
    private String result;

    public Weather (String zip){
        this.zip = zip;
    }

    public void fetch(){

        try {
            String urlString = "https://api.aerisapi.com/observations/"
            + URLEncoder.encode(zip, "utf-8")
            + "?client_id=esmSkJlG1gyDXdQdvUbPd&client_secret=LTvAZ1JGQPpvurUMBzFEm0i5kpTnHtfA3BR497E2";
        URL weatherURl = new URL(urlString);

            InputStream is = weatherURl.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            jse = JsonParser.parseReader(br);


        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getTemp() {


            return jse.getAsJsonObject().
                    get("response").getAsJsonObject().
                    get("ob").getAsJsonObject().
                    get("tempF").getAsInt();

        }

    public String getCityLocation() {
        return jse.getAsJsonObject().
                get("response").getAsJsonObject().
                get("place").getAsJsonObject().
                get("city").getAsString();
    }

    public String getStateLocation() {
        return jse.getAsJsonObject().
                get("response").getAsJsonObject().
                get("place").getAsJsonObject().
                get("state").getAsString().toUpperCase();
    }

    public String getWeatherCondition(){

        return jse.getAsJsonObject().
                get("response").getAsJsonObject().
                get("ob").getAsJsonObject().
                get("weather").getAsString();

    }

    public boolean validZip(){
        return jse.getAsJsonObject().get("error").isJsonNull();

    }

    public String description(){
        String s = (jse.getAsJsonObject().get("error").getAsJsonObject().
                get("description").getAsString());

       return s;
    }
}
