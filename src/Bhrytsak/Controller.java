package Bhrytsak;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    TextField zipCodeText;
    Weather weather;

    @FXML
    Label zipLabel, cityLabel, stateLabel, weatherCondition, discriptionBox;


    public void handleFetchButton(ActionEvent e){


        String zip = zipCodeText.getText();
        weather = new Weather(zip);
        weather.fetch();
        if(weather.validZip()){
            updateWeather();
        }
        else {
            discriptionBox.setText(weather.description());
        }


    }

    public void updateWeather() {
        discriptionBox.setText("");
        zipLabel.setText(""+ weather.getTemp()+ "°");
        cityLabel.setText(weather.getCityLocation());
        stateLabel.setText(weather.getStateLocation());
        weatherCondition.setText(weather.getWeatherCondition());

    }

}
